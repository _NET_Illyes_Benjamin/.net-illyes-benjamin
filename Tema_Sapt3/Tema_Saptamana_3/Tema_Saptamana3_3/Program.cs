﻿/*Write an application that uses random number generation to create sentences. Use four arrays of strings called article, noun, verb and preposition.
Create a sentence by selecting a word at random from each array in the following order: article, noun, verb, preposition, article and noun.
As each word is picked, concatenate it to the previous words in the sentence. The words should be separated by spaces. When the final sentence is output, it should start with a capital letter and end with a period.
The program should generate 20 sentences and output them to a text area.
The arrays should be filled as follows: the article array should contain the articles "the", "a", "one", "some" and "any"; the noun array should contain the nouns "boy", "girl", "dog", "town" and "car";
the verb array should contain the verbs "drove", "jumped", "ran", "walked" and "skipped"; the preposition array should contain the prepositions "to", "from", "over", "under" and "on".
After the preceding program is written, modify the program to produce a short story consisting of several of these sentences.
 */
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema_Saptamana3_3
{
    class Program
    {
        class BasicWords
        {
            private Random rand;

            public string[] Article;
            public string[] Noun;
            public string[] Verb;
            public string[] Preposition;
            public BasicWords()
            {
                Article     = new string[] { "the", "a", "one", "some", "any", };
                Noun        = new string[] { "boy", "girl", "dog", "town", "car", };
                Verb        = new string[] { "drove", "jumped", "ran", "walked", "skipped", };
                Preposition = new string[] { "to", "from", "over", "under", "on", };
                rand = new Random();
            }
            public string SentenceBuilder()
            {
                string word1 = RandomWord("Article");
                word1 = Capitalize(word1);
                string word2 = RandomWord("Noun");
                string word3 = RandomWord("Verb");
                string word4 = RandomWord("Preoposition");
                string word5 = RandomWord("Article");
                string word6 = RandomWord("Noun");

                return String.Format("{0} {1} {2} {3} {4} {5}", word1, word2, word3, word4, word5, word6);
            }
            public string Capitalize(string Word)
            {
                char capitalLetter = ' ';
                string newWord = "";

                capitalLetter = Convert.ToChar(Word.Substring(0, 1));
                newWord = capitalLetter.ToString().ToUpper() + Word.Substring(1);
                return newWord;
            }
            public string RandomWord(string arrayName)
            {
                switch(arrayName)
                {
                    case "Article":
                        return Article[rand.Next(0, 5)];
                    case "Noun":
                        return Noun[rand.Next(0, 5)];
                    case "Verb":
                        return Verb[rand.Next(0, 5)];
                    case "Preposition":
                        return Preposition[rand.Next(0, 5)];
                    default:
                        return "Something went wrong";
                }
            }
        }
        static void Main(string[] args)
        {
            BasicWords sentence = new BasicWords();
            string[] words = new string[6];
            Console.WriteLine(words);
            Console.ReadLine();
        }
    }
}
