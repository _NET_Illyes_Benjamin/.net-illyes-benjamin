﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Person
    {
        public string Firstname;
        public string Surname;
        public Person(string first,string last)
        {
            Firstname = first;
            Surname = last;
        }
        public Person(string name)
        {
            string[] Fullname = name.Split(' ');
            Firstname = Fullname[0] + " " + Fullname[1];
            Surname = Fullname[2];
        }
        static void Main(string[] args)
        {
            Person p1 = new Person("Benjamin", "Illyes");
            Person p2 = new Person("Benjamin Bertalan Illyes");
            Console.WriteLine(p1.Firstname + " " + p1.Surname);
            Console.WriteLine(p2.Firstname);
            Console.ReadLine();
        }
    }
}
