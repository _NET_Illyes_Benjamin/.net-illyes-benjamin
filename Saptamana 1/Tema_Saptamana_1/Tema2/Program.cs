﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema2
{
    class Tank : IDisposable
    {
        public int level; //from 0 to 100


        public Tank()
        {
            level = 0;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
            Dispose();
            GC.SuppressFinalize(this);
        }
        public void Fill(int t)
        {
            if (t <= 100)
            {
                for(int i = t; i<=100; i++)
                {
                    t = i;
                    Console.WriteLine("The tank is currently filling. The level is {0}", t);
                }
            }
        }
        public void FillTo(int p,int t)
        {
            if (p <= 100)
            {
                for (int i = p; i <= t; i++)
                {
                    p = i;
                    Console.WriteLine("The tank is currently filling. The level is {0}", p);
                }
            }
        }
        public void Empty(int t)
        {
            if (t >= 0)
            {
                for (int i = t; i >= 0; i--)
                {
                    t = i;
                    Console.WriteLine("The tank is currently emptying. The level is {0}", t);
                }
            }
        }
        public void EmptyTo(int p,int t)
        {
            if (t >= 0)
            {
                for (int i = t; i >= 0; i--)
                {
                    p = i;
                    Console.WriteLine("The tank is currently emptying. The level is {0}", p);
                }
            }
        }
        //protected virtual void Dispose(bool disposing)
        //{
        //   if (disposing)
        //    {
        //        Dispose();
        //    }
        //}

        static void Main(string[] args)
        {
            Tank t = new Tank();
            Console.WriteLine(t.level);
            t.FillTo(t.level,40);
            t.Fill(t.level);
            t.EmptyTo(t.level, 40);
            t.Empty(t.level);
            if (t.level == 0)
            {
                //bool disposed = true;
                t.Dispose();
            }
        }
    }
}
