﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tema3
{
    class MiniProgram
    {
        int numarator;
        static int numaratorStatic;
        public MiniProgram(int asta)
        {
            Console.WriteLine("A fost chemat constructorul normal");
            Console.ReadLine();
            numarator = asta;
            Console.WriteLine("A fost chemat proprietatea normala");
            Console.ReadLine();
            numaratorStatic = asta;
            Console.WriteLine("A fost chemat proprietatea statica");
            Console.ReadLine();
        }
        static MiniProgram()
        {
            Console.WriteLine("A fost chemat constructorul static");
            Console.ReadLine();

        }
        static void Main(string[] args)
        {
            MiniProgram m = new MiniProgram(6);

        }
    }
}
